"use strict"

$(document).ready(function() {
	var DECK_SIZE = 52;
	var CARDS_COUNT = 12;

	var cardsleft = CARDS_COUNT;

	var deck = new Deck(DECK_SIZE);

	var game = new Game ([deck], CARDS_COUNT);
	game.scoreCallback = updateScoreView
	var $gameView = $('#game');
	var cardViews = createCardViews(game.cards);
	drawCardViewsAtElement(cardViews, $gameView);

	function clickHandler() {
		var card = game.cards[this.id];
		card.isOpen = true;
		game.pushCard(card, updateView);

		$(this).prop("disabled", true);
		$(this).toggleClass("closed open");
		$(this).html(card.conent);
	}
	
	function updateView() {
		disableAllCardsViews();
		setTimeout(closeOpened, 500);
	}

	function disableAllCardsViews(){
		for (var $cardView of cardViews) {
			$($cardView).prop("disabled", true);
		}
	}

	function closeOpened() {
		var cardsleft = CARDS_COUNT;
		for (var $cardView of cardViews) {
			var id = $($cardView).attr('id');
			var card = game.cards[id];
			if (card.isMatched == true) {
				$($cardView).prop("disabled", true);
				cardsleft=cardsleft-1;
			} else {
				$($cardView).prop("disabled", false);
				if (card.isOpen) {
					card.isOpen = false;
					$($cardView).toggleClass("closed open");
					$($cardView).html("");
				}
			}
		}
		if (cardsleft < 5){
			isPaired();
		}
	}

	function updateScoreView(points) {
		$('#points').html(points);
	}
	
	function createCardViews(cards){
		var cardViews = [];
		for(var card of cards ) {
			var id = cards.indexOf(card);
			var $cardView = $('<button/>', { 
				id: id, class: 'card closed', click: clickHandler
			});
			cardViews.push($cardView);
		}
		return cardViews;
	}

	function drawCardViewsAtElement(cardViews, $gameView) {
		for (var $cardView of cardViews) {
			$gameView.append($cardView);
		}
		var $div = $('<div/>', {css: {'clear':'both'}});
		$gameView.append($div);
	}

	function isPaired (){
		var endleftCard={};
		for (var l of game.cards){
			if(!l.isMatched){
				if(isNaN(endleftCard[l.suit])){
					endleftCard[l.suit] =1;
				}
				else{
					endleftCard[l.suit]+=1;
				}
				if(isNaN(endleftCard[l.rank])){
					endleftCard[l.rank] = 1;
				}
				else{
					endleftCard[l.rank]+=1;
				}
			}
		}
		if(Math.max(...Object.values(endleftCard))<2){
			disableAllCardsViews();
			alert("Game Over!");
		}
	}
});
