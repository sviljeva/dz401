//game.js

"use strict"


function Game(decks, cardsCount) {
	this.cards = [];
	this.score = 0.0;
	this.scoreCallback = null;

	var openCards = [];

	while(this.cards.length < cardsCount) {
		var deckIndex = this.cards.length % decks.length;
		var deck = decks[deckIndex];
		var card = deck.getRandomCard();
		if (card != null) {
			this.cards.push(card);
		}
	}

	this.pushCard = function(card, callback) {
		openCards.push(card);
		if (openCards.length == 2) {
			var firstCard = openCards.pop();
			var secondCard = openCards.pop();

			var points = firstCard.matches(secondCard)
			this.addPoints(points);
			if (points > 0) {
				firstCard.isMatched = true;
				secondCard.isMatched = true;
			}
			callback();
		}
	}

	this.addPoints = function(points) {
		this.score += points;
		if (typeof this.scoreCallback === "function") {
			this.scoreCallback(this.score);
		}
	}
}