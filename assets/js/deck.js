//deck.js

"use strict"

function Deck(count) {
	this.cards = [];
	
	var suites = ["♠", "♣", "♥", "♦"];
	var ranks = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
	
	if (count > suites.length * ranks.length){
		count = suites.length * rank.length;
	}
	
	//count = 14 // 28
	for (var i = 0; i < count; ++i) {
		var suitsIndex = Math.floor(i / ranks.length);
		var suit = suites[suitsIndex];
		
		var rankIndex = i % ranks.length;
		var rank = ranks[rankIndex];
		
		var card = new PlayingCard(suit, rank);
		this.cards.push(card);	
	}
	this.getRandomCard = function() {
		var length = this.cards.length;
		if (length == 0) {
			return null;
		}
		var randomIndex = Math.floor(Math.random() * length);
		var card = this.cards[randomIndex];
		this.cards.splice(randomIndex, 1);
		return card;
	}
}