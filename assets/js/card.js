//Card.js
"use strict"
/* Card constructor creates a card
*multiline detail description
*@param isOpen - property if card is facing up
*@param idMatched - property if card is matched with other card
*/


function Card(isOpen, isMatched, conent) {

	this.isOpen = isOpen;
	this.isMatched = isMatched;
	this.conent = conent;
	
	this.matches = function(card) {
		return this.conent === card.conent;
	}
}

function PlayingCard(suit, rank) {
	
	var MatchTypeEnum = {None:-1, Suit:2, Rank: 3}

	Card.call(this);
	this.isOpen = false;
	this.isMatched = false;
	
	this.suit = suit;
	this.rank = rank;
	
	this.conent = this.suit + this.rank;
	
	this.matches = function(card) {
		var score = MatchTypeEnum.None;
		if (this.suit === card.suit) {
			score += MatchTypeEnum.Suit
		}
		if (this.rank === card.rank) {
			score += MatchTypeEnum.Rank
		}
		return score;

	}
	this.matchcard = function(card){
		if (this.suit === card.suit) {
			return true;
		}
		if (this.rank === card.rank) {
			return true;
		}
		return false;
	}
}